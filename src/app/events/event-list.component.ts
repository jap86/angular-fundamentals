import {Component, OnInit} from '@angular/core';
import {EventService} from './shared/event.service';
import {ToastrService} from '../common/toastr.service';


@Component({
  selector: "event-list",
  template : `
    <div>
      <h1>Upcoming Angular Events</h1>
    <hr/>
      <div class="row">
        <div *ngFor="let event of events" class="col-md-5">
          <event-thumbnail #thumbnail (click)="handleThumbnailClick(event.name)"[event]="event"></event-thumbnail>
        </div>
      </div>
    </div>
  `
})
export class EventListComponent implements OnInit{
  events: any[] | undefined;

  constructor(private eventService: EventService, private toastr: ToastrService) {}

  ngOnInit(): void {
    this.events = this.eventService.getEvents();
  }

  handleThumbnailClick(eventName: any): void {
    this.toastr.success(eventName);
  }
}
